//
//  UIView+Extension.swift
//  Yummie
//
//  Created by 1 on 22.03.2022.
//

import UIKit

extension UIView {
   @IBInspectable var cornerRadius: CGFloat {
        get { return cornerRadius }
        set {
            self.layer.cornerRadius = newValue
        }
    }
}

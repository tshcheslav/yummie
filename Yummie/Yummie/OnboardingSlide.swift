//
//  OnboardingSlide.swift
//  Yummie
//
//  Created by 1 on 22.03.2022.
//

import UIKit


struct OnboardingSlide {
    let title: String
    let description: String
    let image: UIImage
}
